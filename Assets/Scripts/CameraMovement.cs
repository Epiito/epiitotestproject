﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField]
    private float maxRotationX = 89.9f;
    [SerializeField]
    private float minRotationX = 269.9f;

    public Camera camChild;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float movementSpeedThisFrame = Time.deltaTime * (float)10;
        Vector3 toAdd = Vector3.zero;
        if (Input.GetKey(KeyCode.W))
        {
            toAdd += (camChild.transform.forward * movementSpeedThisFrame);
        }
        if (Input.GetKey(KeyCode.S))
        {
            toAdd += (-camChild.transform.forward * movementSpeedThisFrame);
        }
        if (Input.GetKey(KeyCode.A))
        {
            toAdd += (-camChild.transform.right * movementSpeedThisFrame);
        }
        if (Input.GetKey(KeyCode.D))
        {
            toAdd += (camChild.transform.right * movementSpeedThisFrame);
        }

        Vector3 toAddY = Vector3.zero;
        if (Input.GetKey(KeyCode.Space))
        {
            toAddY += (transform.up * movementSpeedThisFrame);
        }
        toAdd += toAddY;
        transform.position += toAdd;

        if (Input.GetMouseButton(1))
        {
            //rotate camera
            float mouseInputX = 2.0f * Input.GetAxis("Mouse X");
            float mouseInputY = 2.0f * Input.GetAxis("Mouse Y");

            //stop rotating further than
            float high = maxRotationX - transform.localEulerAngles.x;
            float low = minRotationX - transform.localEulerAngles.x;

            if (mouseInputX > mouseInputY || -mouseInputX > -mouseInputY)
            {
                transform.Rotate(new Vector3(0, mouseInputX, 0));
            }
            if (mouseInputY > mouseInputX || -mouseInputY > -mouseInputX)
            {
                camChild.transform.Rotate(new Vector3(-mouseInputY, 0, 0));
            }
            if (Mathf.Abs(high) < Mathf.Abs(low))
            {
                if (camChild.transform.localEulerAngles.y != 0)
                {
                    camChild.transform.localEulerAngles = new Vector3(maxRotationX, 0, 0);
                }
            }
            else
            {
                if (camChild.transform.localEulerAngles.y != 0)
                {
                    camChild.transform.localEulerAngles = new Vector3(minRotationX, 0, 0);
                }
            }
        }
    }
}
